#ifndef ALG_H
#define ALG_H

int Karatsuba(int A, int B);
int expEuclid(int A, int B);
int Montgomery(int A, int B, int N);
int probabilisticMethod();
int raiseMethod(int x);
int cheneese1(int t, int* a, int* p, int* k);
int cheneese2(int t, int* a, int* p, int* k);

#endif // ALG_H
