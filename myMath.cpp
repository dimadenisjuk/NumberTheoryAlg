#include "myMath.h"

myBinVec initBinVec(int length) {
	myBinVec vec;
	vec.length = length;
	vec.vec = new int[length];
	for(int i = 0; i < length; i++)
		vec.vec[i] = 0;
	return vec;
}

void destroyBinVec(myBinVec vec) {
	delete vec.vec;
}

myBinVec numToBinVec(int num, int length) {
	myBinVec vec = initBinVec(length);
	for(int i = 0; num != 0; i++, num /= 2)
		vec.vec[length - 1 - i] = num % 2;
	return vec;
}

char* binVecToStr(myBinVec vec) {
	char* result = new char[vec.length + 1];
	for(int i = 0; i < vec.length; i++)
		result[i] = vec.vec[i] + 48;
	result[vec.length] = 0;
	return result;
}

int myBinLen(int number) {
	int len = 0;
	for (; number != 0; len++, number/=2);
	return len;
}

int myMax(int first, int second) {
	return first >= second ? first : second;
}

int myPow(int number, int power) {
	int result = 1;
	for(int i = 0; i < power; i++)
		result *= number;
	return result;
}

int myRand() {
	return 1;
}
