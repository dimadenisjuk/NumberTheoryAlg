#ifndef MYMATH_H
#define MYMATH_H

struct myBinVec {
	int length;
	int* vec;
};

myBinVec initBinVec(int length);
void destroyBinVec(myBinVec vec);
myBinVec numToBinVec(int num, int length);
char* binVecToStr(myBinVec vec);

int myMax(int first, int second);
int myBinLen(int number); // binary length
int myPow(int number, int power);
int myRand();

#endif //MYMATH_H
