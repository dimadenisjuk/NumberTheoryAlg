#include "alg.h"
#include "myMath.h"
int Karatsuba(int A, int B) {
	// n = 2^k = 2*t = max length of A and B
	int n = myMax(myBinLen(A), myBinLen(B));
	int t = n / 2;
	int bitMask = 0;
	for(int i = 0, j = 1; i < t; i++, j *= 2)
		bitMask += j;
	int A0 = A & bitMask;
	int A1 = (A & ~bitMask) >> t;
	int B0 = B & bitMask;
	int B1 = (B & ~bitMask) >> t;
	int result = myPow(2, 2 * t) * A1 * B1 + myPow(2, t) * A1 * B1 + myPow(2, t) * (A0 - A1) * (B1 - B0) + myPow(2, t) * A0 * B0 + A0 * B0;
	return result;
}
