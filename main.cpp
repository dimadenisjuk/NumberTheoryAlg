#include "alg.h"
#include <iostream>
using namespace std;

int main() {
	cout << expEuclid(12, 24) << endl;
	cout << Karatsuba(5, 6) << endl;
	cout << Montgomery(3, 4, 101) << endl;	
	return 0;
}
