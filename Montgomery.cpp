#include "alg.h"
#include "myMath.h"
#include <iostream>
// NOTE: works ONLY for ODD N!!!
// returns (A * B) % N
int Montgomery(int A, int B, int N) {
	// TODO retrieve C = (2 ^ (2*n)) % N
	// TODO retrieve A and B as binary vectors and n as their max length
	// int n = myMax(myBinLen(A), myBinLen(B)), R = 0, C = (myPow(2, 2 * n)) % N;
	int n = myBinLen(N), R = 0, C = (myPow(2, 2 * n)) % N;
	myBinVec a = numToBinVec(A, n);
	A %= N;
	B %= N;
	for (int i = 0; i < n; i++) {
		if (a.vec[n - i - 1] == 1)
			R += B;
		if (R % 2 == 1)
			R += N;
		R >>= 1;
	}
	if (R >= N)
		R -= N;
	A = R, B = C, R = 0;
	destroyBinVec(a);
	a = numToBinVec(A, n);
	for (int i = 0; i < n; i++) {
		if (a.vec[n - i - 1] == 1)
			R += B;
		if (R % 2 == 1)
			R += N;
		R >>= 1;
	}
	if (R >= N)
		R -= N;
	return R;
}
