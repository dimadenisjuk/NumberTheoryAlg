#include "alg.h"

int expEuclid(int A, int B) {
	int g = 1, A1 = A, B1 = B;
	while (A1 == 0 && B1 == 0) {
		A1 /= 2;
		B1 /= 2;
		g *= 2;
	}
	int x = A1, y = B1, E = 1, F = 0, G = 0, H = 1;
	while (x != 0) {
		while (x == 0) {
			x /= 2;
			if (E == 0 && F == 0)
				E /= 2, F /= 2;
			else
				E = (E + B1) / 2, F = (F - A1) / 2;
		}
		while (y == 0) {
			y /= 2;
			if(G == 0 && H == 0)
				G /= 2, H /= 2;
			else
				G = (G + B1) / 2, H = (H - A1) / 2;
		}
		if (x >= y)
			x -= y, E -= G, F -= H;
		else
			y -= x, G -= E, H -= F;
	}
	int NOD = g * y, u = G, v = H;
	return NOD;
}
