#include "alg.h"
#include "myMath.h"

// system: x = a[i] % (p[i] ^ k[i])
int chineese1(int t, int* a, int* p, int* k) {
	int aNum = 0, N = 1;
	int* m = new int[t];
	int* u = new int[t];
	for (int i = 0; i < t; i++)
		N *= p[i];
	for (int i = 0; i < t; i++)
		m[i] = N / myPow(p[i], k[i]);
	for (int i = 0; i < t; i++)
	{
		u[i] = (1 / m[i]) % myPow(p[i], k[i]);
		aNum = (aNum + (a[i] * u[i] * m[i]) % N);
	}
	delete[] m;
	delete[] u;
	return aNum;
}

// system: x = a[i] % (p[i] ^ k[i])
int chineese2(int t, int* a, int* p, int* k) {
	int c = 1, aNum = a[1], d, q;
	for (int i = 1; i < t; i++) {
		c = c * myPow(p[i], k[i]);
		d = (1 / c) % myPow(p[i+1], k[i+1]);
		q = (d * (a[i+1] - aNum)) % myPow(p[i+1], k[i+1]);
		a += q * c;
	}
	return aNum;
}
